export class PhoneModel {
    brand: string;
    model: string;
    img: string;

    constructor(brand: string, model: string, img: string) {
        this.brand = brand;
        this.model = model;
        this.img = img;
    }

}