import { Component } from '@angular/core';
import { DemoService } from 'app/demo.service';
import { PhoneModel } from 'app/model/phone.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'app works!';
  user: string = "Erhan";
  dataBinding: string = "-";
  brand: string;
  model: string;

  products: PhoneModel[] = [
    { "brand": "apple", "model": "6S", "img": "https://kozbon.com/wp-content/uploads/2016/12/pamuk-emocan.jpg" },
    { "brand": "samsung", "model": "Galaxy A", "img": "https://kozbon.com/wp-content/uploads/2016/12/pamuk-emocan.jpg" },
    new PhoneModel("sony", "xperia sola", "https://kozbon.com/wp-content/uploads/2016/12/pamuk-emocan.jpg")
  ]

  constructor(private demoService: DemoService) { }

  public loggedIn(): boolean {
    return false;
  }

  logProduct(p: PhoneModel) {
    console.log(p.brand + " - " + p.model);
  }

  changed() {
    console.log(this.dataBinding);
  }

  addProduct() {
    let phone: PhoneModel = new PhoneModel(this.brand, this.model, "");
    this.products.push(phone);
  }

  remove(p) {
    let index = this.products.indexOf(p);

    if (index > -1) {
      this.products.splice(index, 1);
    }

  }

}
