import { AvedisPage } from './app.po';

describe('avedis App', function() {
  let page: AvedisPage;

  beforeEach(() => {
    page = new AvedisPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
